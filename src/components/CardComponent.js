import "./CardComponent.module.css";
import LoginForm from "./LoginForm";

const CardComponent = () => {
    return(
        <div className="card">
            <LoginForm />
        </div>
    );
}

export default CardComponent;