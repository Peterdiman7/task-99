import "./Input.module.css";

const Input = () => {
    return(
    <>
    <label htmlFor="email">Email</label>
    <input type="email" />
    <label htmlFor="password">Password</label>
    <input type="password" />
    </>
    );
}

export default Input;