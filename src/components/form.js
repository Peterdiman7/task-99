import Button from "./Button";
import Input from "./Input";
import "./FormComponent.module.css";

const form = () => {
    return(
        <form>
            <Input />
            <Button />
        </form>
    );
}

export default form;